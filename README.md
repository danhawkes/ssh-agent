# ssh-agent

_SSH-agent in a container. For one-off commands in CI pipelines such as
GitLab/Bitbucket CI, drone.io._

## Usage

Mount your key at `/key`, or set the `KEY` env var:

```sh
docker run -v "~/.ssh/id_rsa:/key" danhawkes/ssh-agent
```

```sh
export KEY=$(cat ~/.ssh/id_rsa)
docker run -e KEY="$KEY" danhawkes/ssh-agent
```

E.g.

```sh
docker run -i -t --rm -e KEY="$KEY" danhawkes/ssh-agent ssh-agent -l
> 2048 SHA256:XuKB3qI74y+CPL3vV66i62SI0j9/H4SXo4CGbmUepCw /key2 (RSA)

docker run -i -t --rm -e KEY="$KEY" danhawkes/ssh-agent ssh root@host 'ls -l /'
> drwxr-xr-x  28 root root  4096 Apr 15 11:14 .
> drwxr-xr-x  28 root root  4096 Apr 15 11:14 ..
> drwxr-xr-x   2 root root 12288 Apr 15 11:14 bin
> drwxr-xr-x   5 root root  3072 Apr 15 11:14 boot
> drwxr-xr-x  23 root root  4260 Apr 15 11:14 dev
```

## Image variants

### `ssh-agent`

The base image, with just `ssh-agent` and `ssh`.

### `rsync-ssh-agent`

As above, but also with `rsync`.
