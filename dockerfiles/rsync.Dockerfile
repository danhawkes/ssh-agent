FROM local/base

RUN apk --no-cache add rsync

CMD rsync
