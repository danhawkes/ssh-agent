FROM alpine:3.7

RUN apk --no-cache add openssh-client && \
  echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > /etc/ssh/ssh_config

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
