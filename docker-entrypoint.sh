#!/bin/sh

if [ ! -e /key ] && [ -z "$KEY" ]; then
  echo "Error: Key does not appear to be mounted at /key, and KEY environment variable not set." >&2
  exit 1
fi

# Create keyfile that's usable by ssh-agent
if [ -e /key ]; then
  cp /key /key2
else
 echo "$KEY" > /key2
fi
chmod 600 /key2

# Start agent and load key
eval "$(ssh-agent -s)" > /dev/null
if ! ssh-add /key2 > /dev/null 2>&1; then
  echo "Error: failed to load key" >&2
  exit 1
fi

exec "$@"
